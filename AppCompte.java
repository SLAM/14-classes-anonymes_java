import java.util.GregorianCalendar;
import java.util.Calendar;
import java.text.DecimalFormat;

public class AppCompte {
    public static void main(String[] args){
        Compte c1 = new Compte("Compte de toto");
        Compte c2 = new Compte("Compte de titi") {
            /**
             * Permet de calculer à tout moment les intérêts produits
             *
             * @return les intérêts produits.
             */
            public double getInterets() {
                // a avec un taux d'intérêt de b% donne d après c ans.
                // d = a * (1 + (b / 100))^c
                Calendar aujourdhui = Calendar.getInstance();
                int nbAnnee = aujourdhui.get(Calendar.YEAR) - this.getDateOuverture().get(Calendar.YEAR);
                return this.getSolde()*Math.pow((1+taux), nbAnnee);
            }

            /**
             * Redéfinit la représentation textuelle en y intégrant les intérêts produits.
             * 
             * @return une chaîne de caractères représentant le compte rémunéré. 
             */
            @Override
            public String toString() {
                DecimalFormat decFormat = new DecimalFormat("0.00");
                Calendar aujourdhui = Calendar.getInstance();
                return super.toString() +
                    "\nIntérêts = " + decFormat.format(this.getInterets()) +
                    " (en " + aujourdhui.get(Calendar.YEAR) + ")";
            }
        };
        c1.deposer(1000);
        c2.setDateOuverture(new GregorianCalendar(2018, 3, 17));
        c2.deposer(800);
        System.out.println(c1);
        System.out.println(c2);
    }
}
