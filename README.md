# Les classes anonymes en Java

Nous revenons pour cela sur le cas des comptes bancaires déjà étudié en TP.
En travaillant sur le concept d'héritage, nous avions précédemment
spécialisé la classe `Compte` dans une classe `CompteRemunere` afin de gérer
notamment les taux d'intérêts produits.

Si nous prenons pour hypothèse, certes peu ambitieuse, que cette spécialisation
ne nous servira qu'une fois dans le cadre de notre programme **AppCompte**,
nous pouvons alors envisager de définir les traitements nécessaires dans une
classe anonyme, qui est à la fois une classe interne à la classe `AppCompte`
et une classe spécialisant la classe `Compte`.

> Retenez cependant que le cas le plus classique d'utilisation de classes
anonymes est pour la spécialisation d'une classe **Abstraite**, voire d'une interface.
L'exemple ci-dessus n'avait pour autre but que de partir d'un cas déjà étudié.
