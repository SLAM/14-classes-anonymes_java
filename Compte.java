import java.util.Calendar;
import java.text.SimpleDateFormat;

public class Compte {
    /** Attributs */
    private String nom;
    private double debit;
    private double credit;
    private int mvtCredit;
    private int mvtDebit;
    private int numero;
    // champ statique privé représentant le compteur de Personnes
    private static int nbInstances;
    public static final float taux = 0.025f;   // constante de classe
    private Calendar dateOuverture;

    /**
     * Un constructeur avec un solde à zéro, et
     * fixant une date d'ouverture à la date du jour.
     *
     * @param nom le nom du compte.
     *
     */
    public Compte(String nom) {
        this.nom = nom;
        //  création d’un nouveau Compte donc incrémentation du compteur
        nbInstances++;
        // affectation au nouveau Compte de son numéro
        numero = nbInstances;
        System.out.println("Création de l’objet : " + numero);
        this.dateOuverture = Calendar.getInstance();
    }

    /**
     * Permet de modifier le nom du compte
     *
     * @param value le nouveau nom du compte.
     */
    public void setNom(String value) {
        this.nom = value;
    }

    /**
     * Permet de connaître la valeur de l'attribut nom
     *
     * @return le nom du compte.
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Permet de diminuer le solde
     *
     * @param value le montant à débiter.
     */
    public void retirer(double value) {
        this.debit += value;
        this.mvtDebit++;
    }

    /**
     * Permet de d'augmenter le solde
     *
     * @param value le montant à créditer
     */
    public void deposer(double value) {
        this.credit += value;
        this.mvtCredit++;
    }

    /**
     * Permet de connaître le nombre d'opérations
     * de retrait.
     *
     * @return le nombre de retraits effectués avec cet objet.
     */
    public int getNbRetraits() {
        return this.mvtDebit;
    }

    /**
     * Permet de connaître le nombre d'opérations
     * de dépôt.
     *
     * @return le nombre de dépôts effectués avec cet objet
     */
    public int getNbDepots() {
        return this.mvtCredit;
    }

    /**
     * Permet de connaître le solde du compte.
     *
     * @return double le solde
     */
    public double getSolde() {
        return this.credit - this.debit;
    }

    /**
     * Permet de connaître la somme des montants déposés.
     *
     * @return double le cumul des crédits.
     */
    public double getCumulCredit() {
        return this.credit;
    }

    /**
     * Permet de connaître la somme des montants retirés.
     *
     * @return double le cumul des débits
     */
    public double getCumulDebit() {
        return this.debit;
    }

    /**
     * méthode statique permettant d’obtenir le nombre d’instances créées
     *
     * @return int nombre d'instances. 
     */
    public static int getNbInstances() {
        return nbInstances;
    }

    /**
     * méthode destructeur.
     */
    public void finalize() {
        //  destruction d’un nouveau Compte donc décrémentation du compteur
        nbInstances--;
        System.out.println("Destruction de l’objet : " + numero);
    }

    /**
     * Permet de modifier la date d'ouverture du compte rémunéré
     *
     * @param date la nouvelle date d'ouverture du compte rémunéré.
     */
    public void setDateOuverture(Calendar date) {
        this.dateOuverture = date;
    }
    /**
     * Permet de connaître la date d'ouverture du compte rémunéré
     *
     * @return la date d'ouverture du compte rémunéré.
     */
    public Calendar getDateOuverture() {
        return this.dateOuverture;
    }

    /**
     * Donne une représentation texte du compte rémunéré.
     * 
     * @return une chaîne de caractères représentant le compte rémunéré. 
     */
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        return "\nNom = " + this.nom +
            "\nSolde = " + this.getSolde() +
            "\nDate ouverture = " + dateFormat.format(this.dateOuverture.getTime());
    }
}
